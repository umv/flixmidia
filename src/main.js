// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";

import { store } from './modules/store';
import { router } from './helpers';

import App from "./App";
import BootstrapVue from "bootstrap-vue";

//import router from "./router";
import VueI18n from "vue-i18n";
import pt_BR from "@/language/pt_BR.json";
import en_US from "@/language/en_US.json";
//import VueProgressBar from "vue-progressbar";
//import KonamiCode from "vue-konami-code";

import Croppa from "vue-croppa";
import VueSweetalert2 from "vue-sweetalert2";



//import { store } from "./store";
import VueResource from "vue-resource";
/*import { library } from "@fortawesome/fontawesome-svg-core";
import { faCoffee } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";*/
import Loading from "vue-loading-overlay";

//import Notifications from "vue-notification";

//import VueAnalytics from "vue-analytics";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "vue-croppa/dist/vue-croppa.css";
import "vue-loading-overlay/dist/vue-loading.css";

Vue.use(Croppa);

/*Vue.use(VueAnalytics, {
  id: "UA-102039631-5"
});*/

window.$ = window.jQuery = require("jquery");

var env = require("@/config.json");

//library.add(faCoffee);

//Vue.component("font-awesome-icon", FontAwesomeIcon);


Vue.config.productionTip = process.env.NODE_ENV === "production";

Vue.use(BootstrapVue);
Vue.use(VueI18n);
//Vue.use(Notifications);
Vue.use(VueResource);
Vue.http.options.emulateJSON = true;
Vue.http.headers = {
  Origin: "https://umv.maxmix.net.br/storage/app/uploads/public/",
  "Access-Control-Allow-Origin": "http://umv.maxmix.com",
  "Access-Control-Allow-Methods": "POST, GET, PUT, OPTIONS, DELETE",
  "Access-Control-Allow-Headers":
    "Access-Control-Allow-Methods, Access-Control-Allow-Origin, Origin, Accept, Content-Type",
  "Content-Type": "application/json",
  Accept: "application/json"
};

// Use bus for transport data
Vue.prototype.$eventBus = new Vue();
Vue.prototype.$debugOn = false;

// Progress bar
/*const options = {
  color: "#bffaf3",
  failedColor: "#874b4b",
  thickness: "5px",
  transition: {
    speed: "0.2s",
    opacity: "0.6s",
    termination: 300
  },
  autoRevert: true,
  location: "left",
  inverse: false
};

Vue.use(VueProgressBar, options);*/

Vue.use(Loading, {
  loader: "spinner",
  width: 150,
  height: 150,
  backgroundColor: "#000000",
  color: "#ffffff"
});

/*Vue.use(KonamiCode, {
  callback: function() {
    let sound =
      "http://soundbible.com/mp3/Elevator Ding-SoundBible.com-685385892.mp3";
    var audio = new Audio(sound);
    audio.play();
    this.$store.commit("changeDebug", true);
  }
});*/

const swalOptions = {
  confirmButtonColor: "#41b882",
  cancelButtonColor: "#ff7674"
};

Vue.use(VueSweetalert2, swalOptions);

const i18n = new VueI18n({
  locale: "pt_BR", // set locale
  fallbackLocale: "en_US",
  messages: {
    pt_BR,
    en_US
  } // set locale messages
});

/* eslint-disable no-new */
export default new Vue({
  el: "#app",
  router,
  store,
  i18n,
  env,
  components: {
    App
  },
  template: "<App/>"
});
