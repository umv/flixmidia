import Vue from 'vue'

export const baseApiUrl = 'https://umv.maxmix.net.br/api/'
export const flixApiUrl = 'https://umv.maxmix.net.br/api/v1/flixmidia'
export const apiStorage = 'https://storage.flixmidia.maxmix.net.br'
export const userKey    = '__flixmidia_user'

export function showError(e) {
    if(e && e.response && e.response.data) {
        Vue.toasted.global.defaultError({ msg : e.response.data })
    } else if(typeof e === 'string') {
        Vue.toasted.global.defaultError({ msg : e })
    } else {
        Vue.toasted.global.defaultError()
    }
}

export default { baseApiUrl, flixApiUrl, apiStorage, showError, userKey }
