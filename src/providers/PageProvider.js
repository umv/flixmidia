import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.http.options.emulateJSON = true
const http = Vue.http

export default {
    prodEnv: require("@/config/prod.env.js"),
    getPages(idMaterial){
        var self = this
        return new Promise((resolve, reject) => {
            //Promise que aguarda resposta da api
            Vue.http.get(self.prodEnv.API_URL + "/pages?material_id="+idMaterial).then(value => {
                if(value.body.status)
                    resolve(value.body.data)
                else
                    reject(value.body)
            });
        })
    }
}