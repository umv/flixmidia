import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.http.options.emulateJSON = true
//Vue.http.headers.common['Access-Control-Request-Method'] = '*'


var cors = require('cors');

export default {
    prodEnv: require("@/config/prod.env.js"),
    getMaterial(id){
        var self = this
        return new Promise((resolve, reject) => {
            //Promise que aguarda resposta da api
            Vue.http.get(self.prodEnv.API_URL + "/material?material_id="+id).then(value => {
                let resp = JSON.stringify(value.body)
                let json_r = JSON.parse(resp)
                resolve(json_r)
            })
        })
    },
    getMaterials(){
        var self = this
        return new Promise((resolve, reject) => {
            //Promise que aguarda resposta da api
            Vue.http.get(self.prodEnv.API_URL + '/materials' ).then(value => {

                if(value.data.status == 1){
                  let resp = value.data.data;
                  resolve(resp);
                }
                reject(reject)
            })
        })
    }
}