import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.http.options.emulateJSON = true
const http = Vue.http

export default {
    prodEnv: require("@/config/prod.env.js"),
    getThemes(idMaterial){
        var self = this
        return new Promise((resolve, reject) => {
            //Promise que aguarda resposta da api
            Vue.http.get(self.prodEnv.API_URL + "/themes?material_id="+idMaterial).then(value => {
                let resp = JSON.stringify(value.data)
                let json_r = JSON.parse(resp)
                resolve(value)
            });
        })
    },
    getTheme(idMaterial, idTheme){
        var self = this
        return new Promise((resolve, reject) => {
            //Promise que aguarda resposta da api
            Vue.http.get(self.prodEnv.API_URL + "/theme?material_id="+idMaterial+"&theme_id="+idTheme).then(value => {
                let resp = JSON.stringify(value.data)
                let json_r = JSON.parse(resp)
                resolve(value)
            });
        })
    }
}