import Vue from 'vue';
import Router from 'vue-router';

import Main from "@/pages/Main";
import Jornal from "@/pages/Jornal";
import Mobile from "@/pages/Mobile";
import Banderole from "@/pages/Banderole";
import Testeira from "@/pages/Testeira";
import Priceposter from "@/pages/Priceposter";
import Login from '@/pages/Login'
import FlixAuth from '../components/flixdovarejo/FlixAuth.vue'

import Swal from "sweetalert2";

Vue.use(Router);

export const router = new Router({
    mode: 'history',
    //base: process.env.BASE_URL,
    routes: [
        {
          path: "/",
          name: "Main",
          component: Main
        },
        {
          path: "/jornal",
          name: "Jornal",
          component: Jornal
        },
        {
          path: '/mobile',
          name: 'Mobile',
          component: Mobile
        },
        {
          path: "/banderole",
          name: "Banderole",
          component: Banderole
        },
        {
          path: "/testeira",
          name: "Testeira",
          component: Testeira
        },
        {
          path: "/priceposter",
          name: "Priceposter",
          component: Priceposter
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/auth/flix',
            name: 'FlixAuth',
            component: FlixAuth
        },

        // otherwise redirect to home
        {
            path: '*',
            redirect: '/'
        }
    ]
});

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login', '/auth/flix'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if(authRequired){

    if(!loggedIn || loggedIn.expires_in < (Date.now() / 1000 | 0)) {

      return next('/login');

    }

  }

  const materialPages = ['/banderole', '/jornal', '/mobile', '/testeira', '/priceposter'];
  const confirmRequired = (materialPages.includes(from.path) && !materialPages.includes(to.path));

  if(confirmRequired && (localStorage.getItem('isModified') && localStorage.getItem('isModified')!="false")){
    Swal.fire({
      title: 'Material ainda não foi salvo',
      text: 'Tem certeza que deseja sair da página? Todo o material não salvo será perdido.',
      type: 'question',
      showCancelButton: true,
      cancelButtonText: 'Voltar',
      //confirmButtonText: 'Tentar novamente'
    }).then((result) => {
      if (result.value) {
        localStorage.setItem('isModified', false);
        next();
      }
    });

  } else {

    next();

  }

  //next();

})
