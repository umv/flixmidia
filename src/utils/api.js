const mocks = {
  'auth': { 'POST': { token: 'This-is-a-mocked-token' } },
  'user/me': { 'GET': { name: 'doggo', title: 'sir' } }
}

const axios = require('axios');

const apiCall = ({url, data, method, ...args}) => new Promise((resolve, reject) => {
  setTimeout(() => {
    try {
      axios.post(`https://umv.maxmix.net.br/api/auth/login?login=${data.username}&password=${data.password}`)
        .then(function (response) {
          resolve(response)
        })
        .catch(function (error) {
          reject(new Error(err))
        });
    } catch (err) {
      reject(new Error(err))
    }
  }, 1000)
})

export default apiCall
