import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import auth from './modules/auth'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    debug: Boolean,
    title: '',
    desc: '',
    price: '',
    image: '',
    index: 0,
    layoutPath: ''
  },
  mutations: {
    changeDebug (state, debug) {
      state.debug = debug
    }
  },
  getters: {
    debug: state => state.debug
  },
  modules: {
    user,
    auth,
  }
})
