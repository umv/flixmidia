import config from '../config';
import { authHeader } from '../helpers';

export const userService = {
    login,
    loginFlix,
    logout,
    getAll
};

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ login: username, password })
    };

    return fetch(`${config.url.api.auth.login}`, requestOptions)
        .then(handleResponse)
        .then(user => {
            // login successful if there's a jwt token in the response
            if (user.token) {
                user.origin_in = 'default';
                user.expires_in = (Date.now() / 1000 | 0) + (1000 * 60 * 60 * 24);
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(user));
            }

            return user;
        });
}

function loginFlix(kc, ka) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ kc: kc, ka, "origem": "flixmidia" })
    };

    return fetch(`${config.url.api.auth.flix}`, requestOptions)
    .then(handleResponse)
    .then(user => {
        // login successful if there's a jwt token in the response
        if (user.token) {
            user.origin_in = 'default';
            user.expires_in = (Date.now() / 1000 | 0) + (1000 * 60 * 60 * 24);
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('user', JSON.stringify(user));
        }

        return user;
    });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
    localStorage.removeItem('results');
}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.url.api.auth.me}`, requestOptions)
    .then(handleResponse)
    .then(user => {
        // login successful if there's a jwt token in the response
        if (user.user) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            const user_old = JSON.parse(localStorage.getItem('user'));
            user_old.user = user.user;
            localStorage.setItem('user', JSON.stringify(user_old));
        }

        return user;
    });
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}
