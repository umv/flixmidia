import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        isLoggedIn: false,

        // Dados do Login do usuário e token
        'token': null,
        'user': null,
    },
    mutations: {
        setUser(state, user) {
            state.user = user
            if (user) {
                axios.defaults.headers.common['Authorization'] = `bearer ${user.token}`
                state.isLoggedIn = true
            } else {
                delete axios.defaults.headers.common['Authorization']
                state.isLoggedIn = false
            }
        }
    }
})
