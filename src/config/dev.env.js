module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
    DEBUG_MODE: true,
    API_URL: 'https://umv.maxmix.net.br/api/v1/flixmidia',
    API_STORAGE: 'https://storage.flixmidia.maxmix.net.br/'
  })


