module.exports = {
    NODE_ENV: '"production"',
    DEBUG_MODE: false,
    API_URL: 'https://umv.maxmix.net.br/api/v1/flixmidia',
    API_STORAGE: 'https://storage.flixmidia.maxmix.net.br'
  }
  